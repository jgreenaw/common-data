def timeZones = java.util.TimeZone.getAvailableIDs();

def header = """
package me.joerayg.locale;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javolution.util.FastSortedMap;

public class TimeZones {

	private static final Map<String, String> timeZonesMap = new FastSortedMap<String, String>();


	 private TimeZones() {
        for (final String timeZone : timeZonesList) {
            timeZonesMap.put(timeZone, timeZone);
        }
    }

    public static List<String> list() {
        return timeZonesList;
    }

    public static Map<String, String> map() {
        return timeZonesMap;
    }


"""
def footer = """



}
"""
def psfs = "public static final String "
def listBegin = """private static final List<String> timeZonesList = Arrays.asList(new String[] { """
def listEnd = "});"
def semi = ";"
def comma = ","
def staticFinals = ""
def equals = " = "
def newLine = "\n"
def quote = '"'
def staticFinalName = ""


timeZones.each{ tz ->
	staticFinalName=tz.replaceAll("/", "_").replaceAll("-", "__").replaceAll("\\+","___")
	staticFinals = staticFinals + psfs + staticFinalName + equals + quote + tz + quote + semi + newLine
	listBegin = listBegin + quote + tz + quote + comma + newLine

}
listBegin = listBegin + listEnd

new File("../../java/me/joerayg/locale/TimeZones.java").withWriter{ it << header+newLine+staticFinals+newLine+listBegin+footer} 

//println listBegin;