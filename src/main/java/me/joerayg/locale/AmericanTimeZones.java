package me.joerayg.locale;

public class AmericanTimeZones {

    public static final String AMERICA_NEWYORK = "America/New_York";
    public static final String AMERICA_DETROIT = "America/Detroit";
    public static final String AMERICA_KY_LOUISVILE = "America/Kentucky/Louisville";
    public static final String AMERICA_KY_MONTICELLO = "America/Kentucky/Monticello";
    public static final String AMERICA_INDIANA_INDIANAPOLIS = "America/Indiana/Indianapolis";
    public static final String AMERICA_INDIANA_VINCENNES = "America/Indiana/Vincennes";
    public static final String AMERICA_INDIANA_WINMAC = "America/Indiana/Winamac";
    public static final String AMERICA_INDIANA_MARENGO = "America/Indiana/Marengo";
    public static final String AMERICA_INDIANA_PETERSBURG = "America/Indiana/Petersburg";
    public static final String AMERICA_INDIANA_VEVAY = "America/Indiana/Vevay";
    public static final String AMERICA_CHICAGO = "America/Chicago";
    public static final String AMERICA_INDIANA_TELL_CITY = "America/Indiana/Tell_City";
    public static final String AMERICA_INDIANA_KNOX = "America/Indiana/Knox";
    public static final String AMERICA_MENOMINEE = "America/Menominee";
    public static final String AMERICA_NORTH_DEKOTA_CENTER = "America/North_Dakota/Center";
    public static final String AMERICA_NORTH_DEKOTA_SALEM = "America/North_Dakota/New_Salem";
    public static final String AMERICA_NORTH_DEKOTA_BEULAH = "America/North_Dakota/Beulah";
    public static final String AMERICA_DENVERK = "America/Denver";
    public static final String AMERICA_BOISE = "America/Boise";
    public static final String AMERICA_SHIPROCK = "America/Shiprock";
    public static final String AMERICA_PHEONIX = "America/Phoenix";
    public static final String AMERICA_LOS_ANGELES = "America/Los_Angeles";
    public static final String AMERICA_ANCHRORAGE = "America/Anchorage";
    public static final String AMERICA_JUNEAU = "America/Juneau";
    public static final String AMERICA_SITKA = "America/Sitka";
    public static final String AMERICA_YAKUTAT = "America/Yakutat";
    public static final String AMERICA_NOME = "America/Nome";
    public static final String AMERICA_ADAK = "America/Adak";
    public static final String AMERICA_METLAKUTLA = "America/Metlakatla";
    public static final String PACIFIC_HONOLULU = "Pacific/Honolulu";

}
