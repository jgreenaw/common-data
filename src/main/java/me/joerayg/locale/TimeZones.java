
package me.joerayg.locale;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javolution.util.FastSortedMap;

public class TimeZones {

	private static final Map<String, String> timeZonesMap = new FastSortedMap<String, String>();


	 private TimeZones() {
        for (final String timeZone : timeZonesList) {
            timeZonesMap.put(timeZone, timeZone);
        }
    }

    public static List<String> list() {
        return timeZonesList;
    }

    public static Map<String, String> map() {
        return timeZonesMap;
    }



public static final String Etc_GMT___12 = "Etc/GMT+12";
public static final String Etc_GMT___11 = "Etc/GMT+11";
public static final String Pacific_Midway = "Pacific/Midway";
public static final String Pacific_Niue = "Pacific/Niue";
public static final String Pacific_Pago_Pago = "Pacific/Pago_Pago";
public static final String Pacific_Samoa = "Pacific/Samoa";
public static final String US_Samoa = "US/Samoa";
public static final String America_Adak = "America/Adak";
public static final String America_Atka = "America/Atka";
public static final String Etc_GMT___10 = "Etc/GMT+10";
public static final String HST = "HST";
public static final String Pacific_Honolulu = "Pacific/Honolulu";
public static final String Pacific_Johnston = "Pacific/Johnston";
public static final String Pacific_Rarotonga = "Pacific/Rarotonga";
public static final String Pacific_Tahiti = "Pacific/Tahiti";
public static final String SystemV_HST10 = "SystemV/HST10";
public static final String US_Aleutian = "US/Aleutian";
public static final String US_Hawaii = "US/Hawaii";
public static final String Pacific_Marquesas = "Pacific/Marquesas";
public static final String AST = "AST";
public static final String America_Anchorage = "America/Anchorage";
public static final String America_Juneau = "America/Juneau";
public static final String America_Nome = "America/Nome";
public static final String America_Sitka = "America/Sitka";
public static final String America_Yakutat = "America/Yakutat";
public static final String Etc_GMT___9 = "Etc/GMT+9";
public static final String Pacific_Gambier = "Pacific/Gambier";
public static final String SystemV_YST9 = "SystemV/YST9";
public static final String SystemV_YST9YDT = "SystemV/YST9YDT";
public static final String US_Alaska = "US/Alaska";
public static final String America_Dawson = "America/Dawson";
public static final String America_Ensenada = "America/Ensenada";
public static final String America_Los_Angeles = "America/Los_Angeles";
public static final String America_Metlakatla = "America/Metlakatla";
public static final String America_Santa_Isabel = "America/Santa_Isabel";
public static final String America_Tijuana = "America/Tijuana";
public static final String America_Vancouver = "America/Vancouver";
public static final String America_Whitehorse = "America/Whitehorse";
public static final String Canada_Pacific = "Canada/Pacific";
public static final String Canada_Yukon = "Canada/Yukon";
public static final String Etc_GMT___8 = "Etc/GMT+8";
public static final String Mexico_BajaNorte = "Mexico/BajaNorte";
public static final String PST = "PST";
public static final String PST8PDT = "PST8PDT";
public static final String Pacific_Pitcairn = "Pacific/Pitcairn";
public static final String SystemV_PST8 = "SystemV/PST8";
public static final String SystemV_PST8PDT = "SystemV/PST8PDT";
public static final String US_Pacific = "US/Pacific";
public static final String US_Pacific__New = "US/Pacific-New";
public static final String America_Boise = "America/Boise";
public static final String America_Cambridge_Bay = "America/Cambridge_Bay";
public static final String America_Chihuahua = "America/Chihuahua";
public static final String America_Creston = "America/Creston";
public static final String America_Dawson_Creek = "America/Dawson_Creek";
public static final String America_Denver = "America/Denver";
public static final String America_Edmonton = "America/Edmonton";
public static final String America_Hermosillo = "America/Hermosillo";
public static final String America_Inuvik = "America/Inuvik";
public static final String America_Mazatlan = "America/Mazatlan";
public static final String America_Ojinaga = "America/Ojinaga";
public static final String America_Phoenix = "America/Phoenix";
public static final String America_Shiprock = "America/Shiprock";
public static final String America_Yellowknife = "America/Yellowknife";
public static final String Canada_Mountain = "Canada/Mountain";
public static final String Etc_GMT___7 = "Etc/GMT+7";
public static final String MST = "MST";
public static final String MST7MDT = "MST7MDT";
public static final String Mexico_BajaSur = "Mexico/BajaSur";
public static final String Navajo = "Navajo";
public static final String PNT = "PNT";
public static final String SystemV_MST7 = "SystemV/MST7";
public static final String SystemV_MST7MDT = "SystemV/MST7MDT";
public static final String US_Arizona = "US/Arizona";
public static final String US_Mountain = "US/Mountain";
public static final String America_Bahia_Banderas = "America/Bahia_Banderas";
public static final String America_Belize = "America/Belize";
public static final String America_Cancun = "America/Cancun";
public static final String America_Chicago = "America/Chicago";
public static final String America_Costa_Rica = "America/Costa_Rica";
public static final String America_El_Salvador = "America/El_Salvador";
public static final String America_Guatemala = "America/Guatemala";
public static final String America_Indiana_Knox = "America/Indiana/Knox";
public static final String America_Indiana_Tell_City = "America/Indiana/Tell_City";
public static final String America_Knox_IN = "America/Knox_IN";
public static final String America_Managua = "America/Managua";
public static final String America_Matamoros = "America/Matamoros";
public static final String America_Menominee = "America/Menominee";
public static final String America_Merida = "America/Merida";
public static final String America_Mexico_City = "America/Mexico_City";
public static final String America_Monterrey = "America/Monterrey";
public static final String America_North_Dakota_Beulah = "America/North_Dakota/Beulah";
public static final String America_North_Dakota_Center = "America/North_Dakota/Center";
public static final String America_North_Dakota_New_Salem = "America/North_Dakota/New_Salem";
public static final String America_Rainy_River = "America/Rainy_River";
public static final String America_Rankin_Inlet = "America/Rankin_Inlet";
public static final String America_Regina = "America/Regina";
public static final String America_Resolute = "America/Resolute";
public static final String America_Swift_Current = "America/Swift_Current";
public static final String America_Tegucigalpa = "America/Tegucigalpa";
public static final String America_Winnipeg = "America/Winnipeg";
public static final String CST = "CST";
public static final String CST6CDT = "CST6CDT";
public static final String Canada_Central = "Canada/Central";
public static final String Canada_East__Saskatchewan = "Canada/East-Saskatchewan";
public static final String Canada_Saskatchewan = "Canada/Saskatchewan";
public static final String Chile_EasterIsland = "Chile/EasterIsland";
public static final String Etc_GMT___6 = "Etc/GMT+6";
public static final String Mexico_General = "Mexico/General";
public static final String Pacific_Easter = "Pacific/Easter";
public static final String Pacific_Galapagos = "Pacific/Galapagos";
public static final String SystemV_CST6 = "SystemV/CST6";
public static final String SystemV_CST6CDT = "SystemV/CST6CDT";
public static final String US_Central = "US/Central";
public static final String US_Indiana__Starke = "US/Indiana-Starke";
public static final String America_Atikokan = "America/Atikokan";
public static final String America_Bogota = "America/Bogota";
public static final String America_Cayman = "America/Cayman";
public static final String America_Coral_Harbour = "America/Coral_Harbour";
public static final String America_Detroit = "America/Detroit";
public static final String America_Eirunepe = "America/Eirunepe";
public static final String America_Fort_Wayne = "America/Fort_Wayne";
public static final String America_Grand_Turk = "America/Grand_Turk";
public static final String America_Guayaquil = "America/Guayaquil";
public static final String America_Havana = "America/Havana";
public static final String America_Indiana_Indianapolis = "America/Indiana/Indianapolis";
public static final String America_Indiana_Marengo = "America/Indiana/Marengo";
public static final String America_Indiana_Petersburg = "America/Indiana/Petersburg";
public static final String America_Indiana_Vevay = "America/Indiana/Vevay";
public static final String America_Indiana_Vincennes = "America/Indiana/Vincennes";
public static final String America_Indiana_Winamac = "America/Indiana/Winamac";
public static final String America_Indianapolis = "America/Indianapolis";
public static final String America_Iqaluit = "America/Iqaluit";
public static final String America_Jamaica = "America/Jamaica";
public static final String America_Kentucky_Louisville = "America/Kentucky/Louisville";
public static final String America_Kentucky_Monticello = "America/Kentucky/Monticello";
public static final String America_Lima = "America/Lima";
public static final String America_Louisville = "America/Louisville";
public static final String America_Montreal = "America/Montreal";
public static final String America_Nassau = "America/Nassau";
public static final String America_New_York = "America/New_York";
public static final String America_Nipigon = "America/Nipigon";
public static final String America_Panama = "America/Panama";
public static final String America_Pangnirtung = "America/Pangnirtung";
public static final String America_Port__au__Prince = "America/Port-au-Prince";
public static final String America_Porto_Acre = "America/Porto_Acre";
public static final String America_Rio_Branco = "America/Rio_Branco";
public static final String America_Thunder_Bay = "America/Thunder_Bay";
public static final String America_Toronto = "America/Toronto";
public static final String Brazil_Acre = "Brazil/Acre";
public static final String Canada_Eastern = "Canada/Eastern";
public static final String Cuba = "Cuba";
public static final String EST = "EST";
public static final String EST5EDT = "EST5EDT";
public static final String Etc_GMT___5 = "Etc/GMT+5";
public static final String IET = "IET";
public static final String Jamaica = "Jamaica";
public static final String SystemV_EST5 = "SystemV/EST5";
public static final String SystemV_EST5EDT = "SystemV/EST5EDT";
public static final String US_East__Indiana = "US/East-Indiana";
public static final String US_Eastern = "US/Eastern";
public static final String US_Michigan = "US/Michigan";
public static final String America_Caracas = "America/Caracas";
public static final String America_Anguilla = "America/Anguilla";
public static final String America_Antigua = "America/Antigua";
public static final String America_Aruba = "America/Aruba";
public static final String America_Asuncion = "America/Asuncion";
public static final String America_Barbados = "America/Barbados";
public static final String America_Blanc__Sablon = "America/Blanc-Sablon";
public static final String America_Boa_Vista = "America/Boa_Vista";
public static final String America_Campo_Grande = "America/Campo_Grande";
public static final String America_Cuiaba = "America/Cuiaba";
public static final String America_Curacao = "America/Curacao";
public static final String America_Dominica = "America/Dominica";
public static final String America_Glace_Bay = "America/Glace_Bay";
public static final String America_Goose_Bay = "America/Goose_Bay";
public static final String America_Grenada = "America/Grenada";
public static final String America_Guadeloupe = "America/Guadeloupe";
public static final String America_Guyana = "America/Guyana";
public static final String America_Halifax = "America/Halifax";
public static final String America_Kralendijk = "America/Kralendijk";
public static final String America_La_Paz = "America/La_Paz";
public static final String America_Lower_Princes = "America/Lower_Princes";
public static final String America_Manaus = "America/Manaus";
public static final String America_Marigot = "America/Marigot";
public static final String America_Martinique = "America/Martinique";
public static final String America_Moncton = "America/Moncton";
public static final String America_Montserrat = "America/Montserrat";
public static final String America_Port_of_Spain = "America/Port_of_Spain";
public static final String America_Porto_Velho = "America/Porto_Velho";
public static final String America_Puerto_Rico = "America/Puerto_Rico";
public static final String America_Santiago = "America/Santiago";
public static final String America_Santo_Domingo = "America/Santo_Domingo";
public static final String America_St_Barthelemy = "America/St_Barthelemy";
public static final String America_St_Kitts = "America/St_Kitts";
public static final String America_St_Lucia = "America/St_Lucia";
public static final String America_St_Thomas = "America/St_Thomas";
public static final String America_St_Vincent = "America/St_Vincent";
public static final String America_Thule = "America/Thule";
public static final String America_Tortola = "America/Tortola";
public static final String America_Virgin = "America/Virgin";
public static final String Antarctica_Palmer = "Antarctica/Palmer";
public static final String Atlantic_Bermuda = "Atlantic/Bermuda";
public static final String Brazil_West = "Brazil/West";
public static final String Canada_Atlantic = "Canada/Atlantic";
public static final String Chile_Continental = "Chile/Continental";
public static final String Etc_GMT___4 = "Etc/GMT+4";
public static final String PRT = "PRT";
public static final String SystemV_AST4 = "SystemV/AST4";
public static final String SystemV_AST4ADT = "SystemV/AST4ADT";
public static final String America_St_Johns = "America/St_Johns";
public static final String CNT = "CNT";
public static final String Canada_Newfoundland = "Canada/Newfoundland";
public static final String AGT = "AGT";
public static final String America_Araguaina = "America/Araguaina";
public static final String America_Argentina_Buenos_Aires = "America/Argentina/Buenos_Aires";
public static final String America_Argentina_Catamarca = "America/Argentina/Catamarca";
public static final String America_Argentina_ComodRivadavia = "America/Argentina/ComodRivadavia";
public static final String America_Argentina_Cordoba = "America/Argentina/Cordoba";
public static final String America_Argentina_Jujuy = "America/Argentina/Jujuy";
public static final String America_Argentina_La_Rioja = "America/Argentina/La_Rioja";
public static final String America_Argentina_Mendoza = "America/Argentina/Mendoza";
public static final String America_Argentina_Rio_Gallegos = "America/Argentina/Rio_Gallegos";
public static final String America_Argentina_Salta = "America/Argentina/Salta";
public static final String America_Argentina_San_Juan = "America/Argentina/San_Juan";
public static final String America_Argentina_San_Luis = "America/Argentina/San_Luis";
public static final String America_Argentina_Tucuman = "America/Argentina/Tucuman";
public static final String America_Argentina_Ushuaia = "America/Argentina/Ushuaia";
public static final String America_Bahia = "America/Bahia";
public static final String America_Belem = "America/Belem";
public static final String America_Buenos_Aires = "America/Buenos_Aires";
public static final String America_Catamarca = "America/Catamarca";
public static final String America_Cayenne = "America/Cayenne";
public static final String America_Cordoba = "America/Cordoba";
public static final String America_Fortaleza = "America/Fortaleza";
public static final String America_Godthab = "America/Godthab";
public static final String America_Jujuy = "America/Jujuy";
public static final String America_Maceio = "America/Maceio";
public static final String America_Mendoza = "America/Mendoza";
public static final String America_Miquelon = "America/Miquelon";
public static final String America_Montevideo = "America/Montevideo";
public static final String America_Paramaribo = "America/Paramaribo";
public static final String America_Recife = "America/Recife";
public static final String America_Rosario = "America/Rosario";
public static final String America_Santarem = "America/Santarem";
public static final String America_Sao_Paulo = "America/Sao_Paulo";
public static final String Antarctica_Rothera = "Antarctica/Rothera";
public static final String Atlantic_Stanley = "Atlantic/Stanley";
public static final String BET = "BET";
public static final String Brazil_East = "Brazil/East";
public static final String Etc_GMT___3 = "Etc/GMT+3";
public static final String America_Noronha = "America/Noronha";
public static final String Atlantic_South_Georgia = "Atlantic/South_Georgia";
public static final String Brazil_DeNoronha = "Brazil/DeNoronha";
public static final String Etc_GMT___2 = "Etc/GMT+2";
public static final String America_Scoresbysund = "America/Scoresbysund";
public static final String Atlantic_Azores = "Atlantic/Azores";
public static final String Atlantic_Cape_Verde = "Atlantic/Cape_Verde";
public static final String Etc_GMT___1 = "Etc/GMT+1";
public static final String Africa_Abidjan = "Africa/Abidjan";
public static final String Africa_Accra = "Africa/Accra";
public static final String Africa_Bamako = "Africa/Bamako";
public static final String Africa_Banjul = "Africa/Banjul";
public static final String Africa_Bissau = "Africa/Bissau";
public static final String Africa_Casablanca = "Africa/Casablanca";
public static final String Africa_Conakry = "Africa/Conakry";
public static final String Africa_Dakar = "Africa/Dakar";
public static final String Africa_El_Aaiun = "Africa/El_Aaiun";
public static final String Africa_Freetown = "Africa/Freetown";
public static final String Africa_Lome = "Africa/Lome";
public static final String Africa_Monrovia = "Africa/Monrovia";
public static final String Africa_Nouakchott = "Africa/Nouakchott";
public static final String Africa_Ouagadougou = "Africa/Ouagadougou";
public static final String Africa_Sao_Tome = "Africa/Sao_Tome";
public static final String Africa_Timbuktu = "Africa/Timbuktu";
public static final String America_Danmarkshavn = "America/Danmarkshavn";
public static final String Antarctica_Troll = "Antarctica/Troll";
public static final String Atlantic_Canary = "Atlantic/Canary";
public static final String Atlantic_Faeroe = "Atlantic/Faeroe";
public static final String Atlantic_Faroe = "Atlantic/Faroe";
public static final String Atlantic_Madeira = "Atlantic/Madeira";
public static final String Atlantic_Reykjavik = "Atlantic/Reykjavik";
public static final String Atlantic_St_Helena = "Atlantic/St_Helena";
public static final String Eire = "Eire";
public static final String Etc_GMT = "Etc/GMT";
public static final String Etc_GMT___0 = "Etc/GMT+0";
public static final String Etc_GMT__0 = "Etc/GMT-0";
public static final String Etc_GMT0 = "Etc/GMT0";
public static final String Etc_Greenwich = "Etc/Greenwich";
public static final String Etc_UCT = "Etc/UCT";
public static final String Etc_UTC = "Etc/UTC";
public static final String Etc_Universal = "Etc/Universal";
public static final String Etc_Zulu = "Etc/Zulu";
public static final String Europe_Belfast = "Europe/Belfast";
public static final String Europe_Dublin = "Europe/Dublin";
public static final String Europe_Guernsey = "Europe/Guernsey";
public static final String Europe_Isle_of_Man = "Europe/Isle_of_Man";
public static final String Europe_Jersey = "Europe/Jersey";
public static final String Europe_Lisbon = "Europe/Lisbon";
public static final String Europe_London = "Europe/London";
public static final String GB = "GB";
public static final String GB__Eire = "GB-Eire";
public static final String GMT = "GMT";
public static final String GMT0 = "GMT0";
public static final String Greenwich = "Greenwich";
public static final String Iceland = "Iceland";
public static final String Portugal = "Portugal";
public static final String UCT = "UCT";
public static final String UTC = "UTC";
public static final String Universal = "Universal";
public static final String WET = "WET";
public static final String Zulu = "Zulu";
public static final String Africa_Algiers = "Africa/Algiers";
public static final String Africa_Bangui = "Africa/Bangui";
public static final String Africa_Brazzaville = "Africa/Brazzaville";
public static final String Africa_Ceuta = "Africa/Ceuta";
public static final String Africa_Douala = "Africa/Douala";
public static final String Africa_Kinshasa = "Africa/Kinshasa";
public static final String Africa_Lagos = "Africa/Lagos";
public static final String Africa_Libreville = "Africa/Libreville";
public static final String Africa_Luanda = "Africa/Luanda";
public static final String Africa_Malabo = "Africa/Malabo";
public static final String Africa_Ndjamena = "Africa/Ndjamena";
public static final String Africa_Niamey = "Africa/Niamey";
public static final String Africa_Porto__Novo = "Africa/Porto-Novo";
public static final String Africa_Tunis = "Africa/Tunis";
public static final String Africa_Windhoek = "Africa/Windhoek";
public static final String Arctic_Longyearbyen = "Arctic/Longyearbyen";
public static final String Atlantic_Jan_Mayen = "Atlantic/Jan_Mayen";
public static final String CET = "CET";
public static final String ECT = "ECT";
public static final String Etc_GMT__1 = "Etc/GMT-1";
public static final String Europe_Amsterdam = "Europe/Amsterdam";
public static final String Europe_Andorra = "Europe/Andorra";
public static final String Europe_Belgrade = "Europe/Belgrade";
public static final String Europe_Berlin = "Europe/Berlin";
public static final String Europe_Bratislava = "Europe/Bratislava";
public static final String Europe_Brussels = "Europe/Brussels";
public static final String Europe_Budapest = "Europe/Budapest";
public static final String Europe_Busingen = "Europe/Busingen";
public static final String Europe_Copenhagen = "Europe/Copenhagen";
public static final String Europe_Gibraltar = "Europe/Gibraltar";
public static final String Europe_Ljubljana = "Europe/Ljubljana";
public static final String Europe_Luxembourg = "Europe/Luxembourg";
public static final String Europe_Madrid = "Europe/Madrid";
public static final String Europe_Malta = "Europe/Malta";
public static final String Europe_Monaco = "Europe/Monaco";
public static final String Europe_Oslo = "Europe/Oslo";
public static final String Europe_Paris = "Europe/Paris";
public static final String Europe_Podgorica = "Europe/Podgorica";
public static final String Europe_Prague = "Europe/Prague";
public static final String Europe_Rome = "Europe/Rome";
public static final String Europe_San_Marino = "Europe/San_Marino";
public static final String Europe_Sarajevo = "Europe/Sarajevo";
public static final String Europe_Skopje = "Europe/Skopje";
public static final String Europe_Stockholm = "Europe/Stockholm";
public static final String Europe_Tirane = "Europe/Tirane";
public static final String Europe_Vaduz = "Europe/Vaduz";
public static final String Europe_Vatican = "Europe/Vatican";
public static final String Europe_Vienna = "Europe/Vienna";
public static final String Europe_Warsaw = "Europe/Warsaw";
public static final String Europe_Zagreb = "Europe/Zagreb";
public static final String Europe_Zurich = "Europe/Zurich";
public static final String MET = "MET";
public static final String Poland = "Poland";
public static final String ART = "ART";
public static final String Africa_Blantyre = "Africa/Blantyre";
public static final String Africa_Bujumbura = "Africa/Bujumbura";
public static final String Africa_Cairo = "Africa/Cairo";
public static final String Africa_Gaborone = "Africa/Gaborone";
public static final String Africa_Harare = "Africa/Harare";
public static final String Africa_Johannesburg = "Africa/Johannesburg";
public static final String Africa_Kigali = "Africa/Kigali";
public static final String Africa_Lubumbashi = "Africa/Lubumbashi";
public static final String Africa_Lusaka = "Africa/Lusaka";
public static final String Africa_Maputo = "Africa/Maputo";
public static final String Africa_Maseru = "Africa/Maseru";
public static final String Africa_Mbabane = "Africa/Mbabane";
public static final String Africa_Tripoli = "Africa/Tripoli";
public static final String Asia_Amman = "Asia/Amman";
public static final String Asia_Beirut = "Asia/Beirut";
public static final String Asia_Damascus = "Asia/Damascus";
public static final String Asia_Gaza = "Asia/Gaza";
public static final String Asia_Hebron = "Asia/Hebron";
public static final String Asia_Istanbul = "Asia/Istanbul";
public static final String Asia_Jerusalem = "Asia/Jerusalem";
public static final String Asia_Nicosia = "Asia/Nicosia";
public static final String Asia_Tel_Aviv = "Asia/Tel_Aviv";
public static final String CAT = "CAT";
public static final String EET = "EET";
public static final String Egypt = "Egypt";
public static final String Etc_GMT__2 = "Etc/GMT-2";
public static final String Europe_Athens = "Europe/Athens";
public static final String Europe_Bucharest = "Europe/Bucharest";
public static final String Europe_Chisinau = "Europe/Chisinau";
public static final String Europe_Helsinki = "Europe/Helsinki";
public static final String Europe_Istanbul = "Europe/Istanbul";
public static final String Europe_Kiev = "Europe/Kiev";
public static final String Europe_Mariehamn = "Europe/Mariehamn";
public static final String Europe_Nicosia = "Europe/Nicosia";
public static final String Europe_Riga = "Europe/Riga";
public static final String Europe_Sofia = "Europe/Sofia";
public static final String Europe_Tallinn = "Europe/Tallinn";
public static final String Europe_Tiraspol = "Europe/Tiraspol";
public static final String Europe_Uzhgorod = "Europe/Uzhgorod";
public static final String Europe_Vilnius = "Europe/Vilnius";
public static final String Europe_Zaporozhye = "Europe/Zaporozhye";
public static final String Israel = "Israel";
public static final String Libya = "Libya";
public static final String Turkey = "Turkey";
public static final String Africa_Addis_Ababa = "Africa/Addis_Ababa";
public static final String Africa_Asmara = "Africa/Asmara";
public static final String Africa_Asmera = "Africa/Asmera";
public static final String Africa_Dar_es_Salaam = "Africa/Dar_es_Salaam";
public static final String Africa_Djibouti = "Africa/Djibouti";
public static final String Africa_Juba = "Africa/Juba";
public static final String Africa_Kampala = "Africa/Kampala";
public static final String Africa_Khartoum = "Africa/Khartoum";
public static final String Africa_Mogadishu = "Africa/Mogadishu";
public static final String Africa_Nairobi = "Africa/Nairobi";
public static final String Antarctica_Syowa = "Antarctica/Syowa";
public static final String Asia_Aden = "Asia/Aden";
public static final String Asia_Baghdad = "Asia/Baghdad";
public static final String Asia_Bahrain = "Asia/Bahrain";
public static final String Asia_Kuwait = "Asia/Kuwait";
public static final String Asia_Qatar = "Asia/Qatar";
public static final String Asia_Riyadh = "Asia/Riyadh";
public static final String EAT = "EAT";
public static final String Etc_GMT__3 = "Etc/GMT-3";
public static final String Europe_Kaliningrad = "Europe/Kaliningrad";
public static final String Europe_Minsk = "Europe/Minsk";
public static final String Indian_Antananarivo = "Indian/Antananarivo";
public static final String Indian_Comoro = "Indian/Comoro";
public static final String Indian_Mayotte = "Indian/Mayotte";
public static final String Asia_Riyadh87 = "Asia/Riyadh87";
public static final String Asia_Riyadh88 = "Asia/Riyadh88";
public static final String Asia_Riyadh89 = "Asia/Riyadh89";
public static final String Mideast_Riyadh87 = "Mideast/Riyadh87";
public static final String Mideast_Riyadh88 = "Mideast/Riyadh88";
public static final String Mideast_Riyadh89 = "Mideast/Riyadh89";
public static final String Asia_Tehran = "Asia/Tehran";
public static final String Iran = "Iran";
public static final String Asia_Baku = "Asia/Baku";
public static final String Asia_Dubai = "Asia/Dubai";
public static final String Asia_Muscat = "Asia/Muscat";
public static final String Asia_Tbilisi = "Asia/Tbilisi";
public static final String Asia_Yerevan = "Asia/Yerevan";
public static final String Etc_GMT__4 = "Etc/GMT-4";
public static final String Europe_Moscow = "Europe/Moscow";
public static final String Europe_Samara = "Europe/Samara";
public static final String Europe_Simferopol = "Europe/Simferopol";
public static final String Europe_Volgograd = "Europe/Volgograd";
public static final String Indian_Mahe = "Indian/Mahe";
public static final String Indian_Mauritius = "Indian/Mauritius";
public static final String Indian_Reunion = "Indian/Reunion";
public static final String NET = "NET";
public static final String W__SU = "W-SU";
public static final String Asia_Kabul = "Asia/Kabul";
public static final String Antarctica_Mawson = "Antarctica/Mawson";
public static final String Asia_Aqtau = "Asia/Aqtau";
public static final String Asia_Aqtobe = "Asia/Aqtobe";
public static final String Asia_Ashgabat = "Asia/Ashgabat";
public static final String Asia_Ashkhabad = "Asia/Ashkhabad";
public static final String Asia_Dushanbe = "Asia/Dushanbe";
public static final String Asia_Karachi = "Asia/Karachi";
public static final String Asia_Oral = "Asia/Oral";
public static final String Asia_Samarkand = "Asia/Samarkand";
public static final String Asia_Tashkent = "Asia/Tashkent";
public static final String Etc_GMT__5 = "Etc/GMT-5";
public static final String Indian_Kerguelen = "Indian/Kerguelen";
public static final String Indian_Maldives = "Indian/Maldives";
public static final String PLT = "PLT";
public static final String Asia_Calcutta = "Asia/Calcutta";
public static final String Asia_Colombo = "Asia/Colombo";
public static final String Asia_Kolkata = "Asia/Kolkata";
public static final String IST = "IST";
public static final String Asia_Kathmandu = "Asia/Kathmandu";
public static final String Asia_Katmandu = "Asia/Katmandu";
public static final String Antarctica_Vostok = "Antarctica/Vostok";
public static final String Asia_Almaty = "Asia/Almaty";
public static final String Asia_Bishkek = "Asia/Bishkek";
public static final String Asia_Dacca = "Asia/Dacca";
public static final String Asia_Dhaka = "Asia/Dhaka";
public static final String Asia_Qyzylorda = "Asia/Qyzylorda";
public static final String Asia_Thimbu = "Asia/Thimbu";
public static final String Asia_Thimphu = "Asia/Thimphu";
public static final String Asia_Yekaterinburg = "Asia/Yekaterinburg";
public static final String BST = "BST";
public static final String Etc_GMT__6 = "Etc/GMT-6";
public static final String Indian_Chagos = "Indian/Chagos";
public static final String Asia_Rangoon = "Asia/Rangoon";
public static final String Indian_Cocos = "Indian/Cocos";
public static final String Antarctica_Davis = "Antarctica/Davis";
public static final String Asia_Bangkok = "Asia/Bangkok";
public static final String Asia_Ho_Chi_Minh = "Asia/Ho_Chi_Minh";
public static final String Asia_Hovd = "Asia/Hovd";
public static final String Asia_Jakarta = "Asia/Jakarta";
public static final String Asia_Novokuznetsk = "Asia/Novokuznetsk";
public static final String Asia_Novosibirsk = "Asia/Novosibirsk";
public static final String Asia_Omsk = "Asia/Omsk";
public static final String Asia_Phnom_Penh = "Asia/Phnom_Penh";
public static final String Asia_Pontianak = "Asia/Pontianak";
public static final String Asia_Saigon = "Asia/Saigon";
public static final String Asia_Vientiane = "Asia/Vientiane";
public static final String Etc_GMT__7 = "Etc/GMT-7";
public static final String Indian_Christmas = "Indian/Christmas";
public static final String VST = "VST";
public static final String Antarctica_Casey = "Antarctica/Casey";
public static final String Asia_Brunei = "Asia/Brunei";
public static final String Asia_Choibalsan = "Asia/Choibalsan";
public static final String Asia_Chongqing = "Asia/Chongqing";
public static final String Asia_Chungking = "Asia/Chungking";
public static final String Asia_Harbin = "Asia/Harbin";
public static final String Asia_Hong_Kong = "Asia/Hong_Kong";
public static final String Asia_Kashgar = "Asia/Kashgar";
public static final String Asia_Krasnoyarsk = "Asia/Krasnoyarsk";
public static final String Asia_Kuala_Lumpur = "Asia/Kuala_Lumpur";
public static final String Asia_Kuching = "Asia/Kuching";
public static final String Asia_Macao = "Asia/Macao";
public static final String Asia_Macau = "Asia/Macau";
public static final String Asia_Makassar = "Asia/Makassar";
public static final String Asia_Manila = "Asia/Manila";
public static final String Asia_Shanghai = "Asia/Shanghai";
public static final String Asia_Singapore = "Asia/Singapore";
public static final String Asia_Taipei = "Asia/Taipei";
public static final String Asia_Ujung_Pandang = "Asia/Ujung_Pandang";
public static final String Asia_Ulaanbaatar = "Asia/Ulaanbaatar";
public static final String Asia_Ulan_Bator = "Asia/Ulan_Bator";
public static final String Asia_Urumqi = "Asia/Urumqi";
public static final String Australia_Perth = "Australia/Perth";
public static final String Australia_West = "Australia/West";
public static final String CTT = "CTT";
public static final String Etc_GMT__8 = "Etc/GMT-8";
public static final String Hongkong = "Hongkong";
public static final String PRC = "PRC";
public static final String Singapore = "Singapore";
public static final String Australia_Eucla = "Australia/Eucla";
public static final String Asia_Dili = "Asia/Dili";
public static final String Asia_Irkutsk = "Asia/Irkutsk";
public static final String Asia_Jayapura = "Asia/Jayapura";
public static final String Asia_Pyongyang = "Asia/Pyongyang";
public static final String Asia_Seoul = "Asia/Seoul";
public static final String Asia_Tokyo = "Asia/Tokyo";
public static final String Etc_GMT__9 = "Etc/GMT-9";
public static final String JST = "JST";
public static final String Japan = "Japan";
public static final String Pacific_Palau = "Pacific/Palau";
public static final String ROK = "ROK";
public static final String ACT = "ACT";
public static final String Australia_Adelaide = "Australia/Adelaide";
public static final String Australia_Broken_Hill = "Australia/Broken_Hill";
public static final String Australia_Darwin = "Australia/Darwin";
public static final String Australia_North = "Australia/North";
public static final String Australia_South = "Australia/South";
public static final String Australia_Yancowinna = "Australia/Yancowinna";
public static final String AET = "AET";
public static final String Antarctica_DumontDUrville = "Antarctica/DumontDUrville";
public static final String Asia_Khandyga = "Asia/Khandyga";
public static final String Asia_Yakutsk = "Asia/Yakutsk";
public static final String Australia_ACT = "Australia/ACT";
public static final String Australia_Brisbane = "Australia/Brisbane";
public static final String Australia_Canberra = "Australia/Canberra";
public static final String Australia_Currie = "Australia/Currie";
public static final String Australia_Hobart = "Australia/Hobart";
public static final String Australia_Lindeman = "Australia/Lindeman";
public static final String Australia_Melbourne = "Australia/Melbourne";
public static final String Australia_NSW = "Australia/NSW";
public static final String Australia_Queensland = "Australia/Queensland";
public static final String Australia_Sydney = "Australia/Sydney";
public static final String Australia_Tasmania = "Australia/Tasmania";
public static final String Australia_Victoria = "Australia/Victoria";
public static final String Etc_GMT__10 = "Etc/GMT-10";
public static final String Pacific_Chuuk = "Pacific/Chuuk";
public static final String Pacific_Guam = "Pacific/Guam";
public static final String Pacific_Port_Moresby = "Pacific/Port_Moresby";
public static final String Pacific_Saipan = "Pacific/Saipan";
public static final String Pacific_Truk = "Pacific/Truk";
public static final String Pacific_Yap = "Pacific/Yap";
public static final String Australia_LHI = "Australia/LHI";
public static final String Australia_Lord_Howe = "Australia/Lord_Howe";
public static final String Antarctica_Macquarie = "Antarctica/Macquarie";
public static final String Asia_Sakhalin = "Asia/Sakhalin";
public static final String Asia_Ust__Nera = "Asia/Ust-Nera";
public static final String Asia_Vladivostok = "Asia/Vladivostok";
public static final String Etc_GMT__11 = "Etc/GMT-11";
public static final String Pacific_Efate = "Pacific/Efate";
public static final String Pacific_Guadalcanal = "Pacific/Guadalcanal";
public static final String Pacific_Kosrae = "Pacific/Kosrae";
public static final String Pacific_Noumea = "Pacific/Noumea";
public static final String Pacific_Pohnpei = "Pacific/Pohnpei";
public static final String Pacific_Ponape = "Pacific/Ponape";
public static final String SST = "SST";
public static final String Pacific_Norfolk = "Pacific/Norfolk";
public static final String Antarctica_McMurdo = "Antarctica/McMurdo";
public static final String Antarctica_South_Pole = "Antarctica/South_Pole";
public static final String Asia_Anadyr = "Asia/Anadyr";
public static final String Asia_Kamchatka = "Asia/Kamchatka";
public static final String Asia_Magadan = "Asia/Magadan";
public static final String Etc_GMT__12 = "Etc/GMT-12";
public static final String Kwajalein = "Kwajalein";
public static final String NST = "NST";
public static final String NZ = "NZ";
public static final String Pacific_Auckland = "Pacific/Auckland";
public static final String Pacific_Fiji = "Pacific/Fiji";
public static final String Pacific_Funafuti = "Pacific/Funafuti";
public static final String Pacific_Kwajalein = "Pacific/Kwajalein";
public static final String Pacific_Majuro = "Pacific/Majuro";
public static final String Pacific_Nauru = "Pacific/Nauru";
public static final String Pacific_Tarawa = "Pacific/Tarawa";
public static final String Pacific_Wake = "Pacific/Wake";
public static final String Pacific_Wallis = "Pacific/Wallis";
public static final String NZ__CHAT = "NZ-CHAT";
public static final String Pacific_Chatham = "Pacific/Chatham";
public static final String Etc_GMT__13 = "Etc/GMT-13";
public static final String MIT = "MIT";
public static final String Pacific_Apia = "Pacific/Apia";
public static final String Pacific_Enderbury = "Pacific/Enderbury";
public static final String Pacific_Fakaofo = "Pacific/Fakaofo";
public static final String Pacific_Tongatapu = "Pacific/Tongatapu";
public static final String Etc_GMT__14 = "Etc/GMT-14";
public static final String Pacific_Kiritimati = "Pacific/Kiritimati";

private static final List<String> timeZonesList = Arrays.asList(new String[] { "Etc/GMT+12",
"Etc/GMT+11",
"Pacific/Midway",
"Pacific/Niue",
"Pacific/Pago_Pago",
"Pacific/Samoa",
"US/Samoa",
"America/Adak",
"America/Atka",
"Etc/GMT+10",
"HST",
"Pacific/Honolulu",
"Pacific/Johnston",
"Pacific/Rarotonga",
"Pacific/Tahiti",
"SystemV/HST10",
"US/Aleutian",
"US/Hawaii",
"Pacific/Marquesas",
"AST",
"America/Anchorage",
"America/Juneau",
"America/Nome",
"America/Sitka",
"America/Yakutat",
"Etc/GMT+9",
"Pacific/Gambier",
"SystemV/YST9",
"SystemV/YST9YDT",
"US/Alaska",
"America/Dawson",
"America/Ensenada",
"America/Los_Angeles",
"America/Metlakatla",
"America/Santa_Isabel",
"America/Tijuana",
"America/Vancouver",
"America/Whitehorse",
"Canada/Pacific",
"Canada/Yukon",
"Etc/GMT+8",
"Mexico/BajaNorte",
"PST",
"PST8PDT",
"Pacific/Pitcairn",
"SystemV/PST8",
"SystemV/PST8PDT",
"US/Pacific",
"US/Pacific-New",
"America/Boise",
"America/Cambridge_Bay",
"America/Chihuahua",
"America/Creston",
"America/Dawson_Creek",
"America/Denver",
"America/Edmonton",
"America/Hermosillo",
"America/Inuvik",
"America/Mazatlan",
"America/Ojinaga",
"America/Phoenix",
"America/Shiprock",
"America/Yellowknife",
"Canada/Mountain",
"Etc/GMT+7",
"MST",
"MST7MDT",
"Mexico/BajaSur",
"Navajo",
"PNT",
"SystemV/MST7",
"SystemV/MST7MDT",
"US/Arizona",
"US/Mountain",
"America/Bahia_Banderas",
"America/Belize",
"America/Cancun",
"America/Chicago",
"America/Costa_Rica",
"America/El_Salvador",
"America/Guatemala",
"America/Indiana/Knox",
"America/Indiana/Tell_City",
"America/Knox_IN",
"America/Managua",
"America/Matamoros",
"America/Menominee",
"America/Merida",
"America/Mexico_City",
"America/Monterrey",
"America/North_Dakota/Beulah",
"America/North_Dakota/Center",
"America/North_Dakota/New_Salem",
"America/Rainy_River",
"America/Rankin_Inlet",
"America/Regina",
"America/Resolute",
"America/Swift_Current",
"America/Tegucigalpa",
"America/Winnipeg",
"CST",
"CST6CDT",
"Canada/Central",
"Canada/East-Saskatchewan",
"Canada/Saskatchewan",
"Chile/EasterIsland",
"Etc/GMT+6",
"Mexico/General",
"Pacific/Easter",
"Pacific/Galapagos",
"SystemV/CST6",
"SystemV/CST6CDT",
"US/Central",
"US/Indiana-Starke",
"America/Atikokan",
"America/Bogota",
"America/Cayman",
"America/Coral_Harbour",
"America/Detroit",
"America/Eirunepe",
"America/Fort_Wayne",
"America/Grand_Turk",
"America/Guayaquil",
"America/Havana",
"America/Indiana/Indianapolis",
"America/Indiana/Marengo",
"America/Indiana/Petersburg",
"America/Indiana/Vevay",
"America/Indiana/Vincennes",
"America/Indiana/Winamac",
"America/Indianapolis",
"America/Iqaluit",
"America/Jamaica",
"America/Kentucky/Louisville",
"America/Kentucky/Monticello",
"America/Lima",
"America/Louisville",
"America/Montreal",
"America/Nassau",
"America/New_York",
"America/Nipigon",
"America/Panama",
"America/Pangnirtung",
"America/Port-au-Prince",
"America/Porto_Acre",
"America/Rio_Branco",
"America/Thunder_Bay",
"America/Toronto",
"Brazil/Acre",
"Canada/Eastern",
"Cuba",
"EST",
"EST5EDT",
"Etc/GMT+5",
"IET",
"Jamaica",
"SystemV/EST5",
"SystemV/EST5EDT",
"US/East-Indiana",
"US/Eastern",
"US/Michigan",
"America/Caracas",
"America/Anguilla",
"America/Antigua",
"America/Aruba",
"America/Asuncion",
"America/Barbados",
"America/Blanc-Sablon",
"America/Boa_Vista",
"America/Campo_Grande",
"America/Cuiaba",
"America/Curacao",
"America/Dominica",
"America/Glace_Bay",
"America/Goose_Bay",
"America/Grenada",
"America/Guadeloupe",
"America/Guyana",
"America/Halifax",
"America/Kralendijk",
"America/La_Paz",
"America/Lower_Princes",
"America/Manaus",
"America/Marigot",
"America/Martinique",
"America/Moncton",
"America/Montserrat",
"America/Port_of_Spain",
"America/Porto_Velho",
"America/Puerto_Rico",
"America/Santiago",
"America/Santo_Domingo",
"America/St_Barthelemy",
"America/St_Kitts",
"America/St_Lucia",
"America/St_Thomas",
"America/St_Vincent",
"America/Thule",
"America/Tortola",
"America/Virgin",
"Antarctica/Palmer",
"Atlantic/Bermuda",
"Brazil/West",
"Canada/Atlantic",
"Chile/Continental",
"Etc/GMT+4",
"PRT",
"SystemV/AST4",
"SystemV/AST4ADT",
"America/St_Johns",
"CNT",
"Canada/Newfoundland",
"AGT",
"America/Araguaina",
"America/Argentina/Buenos_Aires",
"America/Argentina/Catamarca",
"America/Argentina/ComodRivadavia",
"America/Argentina/Cordoba",
"America/Argentina/Jujuy",
"America/Argentina/La_Rioja",
"America/Argentina/Mendoza",
"America/Argentina/Rio_Gallegos",
"America/Argentina/Salta",
"America/Argentina/San_Juan",
"America/Argentina/San_Luis",
"America/Argentina/Tucuman",
"America/Argentina/Ushuaia",
"America/Bahia",
"America/Belem",
"America/Buenos_Aires",
"America/Catamarca",
"America/Cayenne",
"America/Cordoba",
"America/Fortaleza",
"America/Godthab",
"America/Jujuy",
"America/Maceio",
"America/Mendoza",
"America/Miquelon",
"America/Montevideo",
"America/Paramaribo",
"America/Recife",
"America/Rosario",
"America/Santarem",
"America/Sao_Paulo",
"Antarctica/Rothera",
"Atlantic/Stanley",
"BET",
"Brazil/East",
"Etc/GMT+3",
"America/Noronha",
"Atlantic/South_Georgia",
"Brazil/DeNoronha",
"Etc/GMT+2",
"America/Scoresbysund",
"Atlantic/Azores",
"Atlantic/Cape_Verde",
"Etc/GMT+1",
"Africa/Abidjan",
"Africa/Accra",
"Africa/Bamako",
"Africa/Banjul",
"Africa/Bissau",
"Africa/Casablanca",
"Africa/Conakry",
"Africa/Dakar",
"Africa/El_Aaiun",
"Africa/Freetown",
"Africa/Lome",
"Africa/Monrovia",
"Africa/Nouakchott",
"Africa/Ouagadougou",
"Africa/Sao_Tome",
"Africa/Timbuktu",
"America/Danmarkshavn",
"Antarctica/Troll",
"Atlantic/Canary",
"Atlantic/Faeroe",
"Atlantic/Faroe",
"Atlantic/Madeira",
"Atlantic/Reykjavik",
"Atlantic/St_Helena",
"Eire",
"Etc/GMT",
"Etc/GMT+0",
"Etc/GMT-0",
"Etc/GMT0",
"Etc/Greenwich",
"Etc/UCT",
"Etc/UTC",
"Etc/Universal",
"Etc/Zulu",
"Europe/Belfast",
"Europe/Dublin",
"Europe/Guernsey",
"Europe/Isle_of_Man",
"Europe/Jersey",
"Europe/Lisbon",
"Europe/London",
"GB",
"GB-Eire",
"GMT",
"GMT0",
"Greenwich",
"Iceland",
"Portugal",
"UCT",
"UTC",
"Universal",
"WET",
"Zulu",
"Africa/Algiers",
"Africa/Bangui",
"Africa/Brazzaville",
"Africa/Ceuta",
"Africa/Douala",
"Africa/Kinshasa",
"Africa/Lagos",
"Africa/Libreville",
"Africa/Luanda",
"Africa/Malabo",
"Africa/Ndjamena",
"Africa/Niamey",
"Africa/Porto-Novo",
"Africa/Tunis",
"Africa/Windhoek",
"Arctic/Longyearbyen",
"Atlantic/Jan_Mayen",
"CET",
"ECT",
"Etc/GMT-1",
"Europe/Amsterdam",
"Europe/Andorra",
"Europe/Belgrade",
"Europe/Berlin",
"Europe/Bratislava",
"Europe/Brussels",
"Europe/Budapest",
"Europe/Busingen",
"Europe/Copenhagen",
"Europe/Gibraltar",
"Europe/Ljubljana",
"Europe/Luxembourg",
"Europe/Madrid",
"Europe/Malta",
"Europe/Monaco",
"Europe/Oslo",
"Europe/Paris",
"Europe/Podgorica",
"Europe/Prague",
"Europe/Rome",
"Europe/San_Marino",
"Europe/Sarajevo",
"Europe/Skopje",
"Europe/Stockholm",
"Europe/Tirane",
"Europe/Vaduz",
"Europe/Vatican",
"Europe/Vienna",
"Europe/Warsaw",
"Europe/Zagreb",
"Europe/Zurich",
"MET",
"Poland",
"ART",
"Africa/Blantyre",
"Africa/Bujumbura",
"Africa/Cairo",
"Africa/Gaborone",
"Africa/Harare",
"Africa/Johannesburg",
"Africa/Kigali",
"Africa/Lubumbashi",
"Africa/Lusaka",
"Africa/Maputo",
"Africa/Maseru",
"Africa/Mbabane",
"Africa/Tripoli",
"Asia/Amman",
"Asia/Beirut",
"Asia/Damascus",
"Asia/Gaza",
"Asia/Hebron",
"Asia/Istanbul",
"Asia/Jerusalem",
"Asia/Nicosia",
"Asia/Tel_Aviv",
"CAT",
"EET",
"Egypt",
"Etc/GMT-2",
"Europe/Athens",
"Europe/Bucharest",
"Europe/Chisinau",
"Europe/Helsinki",
"Europe/Istanbul",
"Europe/Kiev",
"Europe/Mariehamn",
"Europe/Nicosia",
"Europe/Riga",
"Europe/Sofia",
"Europe/Tallinn",
"Europe/Tiraspol",
"Europe/Uzhgorod",
"Europe/Vilnius",
"Europe/Zaporozhye",
"Israel",
"Libya",
"Turkey",
"Africa/Addis_Ababa",
"Africa/Asmara",
"Africa/Asmera",
"Africa/Dar_es_Salaam",
"Africa/Djibouti",
"Africa/Juba",
"Africa/Kampala",
"Africa/Khartoum",
"Africa/Mogadishu",
"Africa/Nairobi",
"Antarctica/Syowa",
"Asia/Aden",
"Asia/Baghdad",
"Asia/Bahrain",
"Asia/Kuwait",
"Asia/Qatar",
"Asia/Riyadh",
"EAT",
"Etc/GMT-3",
"Europe/Kaliningrad",
"Europe/Minsk",
"Indian/Antananarivo",
"Indian/Comoro",
"Indian/Mayotte",
"Asia/Riyadh87",
"Asia/Riyadh88",
"Asia/Riyadh89",
"Mideast/Riyadh87",
"Mideast/Riyadh88",
"Mideast/Riyadh89",
"Asia/Tehran",
"Iran",
"Asia/Baku",
"Asia/Dubai",
"Asia/Muscat",
"Asia/Tbilisi",
"Asia/Yerevan",
"Etc/GMT-4",
"Europe/Moscow",
"Europe/Samara",
"Europe/Simferopol",
"Europe/Volgograd",
"Indian/Mahe",
"Indian/Mauritius",
"Indian/Reunion",
"NET",
"W-SU",
"Asia/Kabul",
"Antarctica/Mawson",
"Asia/Aqtau",
"Asia/Aqtobe",
"Asia/Ashgabat",
"Asia/Ashkhabad",
"Asia/Dushanbe",
"Asia/Karachi",
"Asia/Oral",
"Asia/Samarkand",
"Asia/Tashkent",
"Etc/GMT-5",
"Indian/Kerguelen",
"Indian/Maldives",
"PLT",
"Asia/Calcutta",
"Asia/Colombo",
"Asia/Kolkata",
"IST",
"Asia/Kathmandu",
"Asia/Katmandu",
"Antarctica/Vostok",
"Asia/Almaty",
"Asia/Bishkek",
"Asia/Dacca",
"Asia/Dhaka",
"Asia/Qyzylorda",
"Asia/Thimbu",
"Asia/Thimphu",
"Asia/Yekaterinburg",
"BST",
"Etc/GMT-6",
"Indian/Chagos",
"Asia/Rangoon",
"Indian/Cocos",
"Antarctica/Davis",
"Asia/Bangkok",
"Asia/Ho_Chi_Minh",
"Asia/Hovd",
"Asia/Jakarta",
"Asia/Novokuznetsk",
"Asia/Novosibirsk",
"Asia/Omsk",
"Asia/Phnom_Penh",
"Asia/Pontianak",
"Asia/Saigon",
"Asia/Vientiane",
"Etc/GMT-7",
"Indian/Christmas",
"VST",
"Antarctica/Casey",
"Asia/Brunei",
"Asia/Choibalsan",
"Asia/Chongqing",
"Asia/Chungking",
"Asia/Harbin",
"Asia/Hong_Kong",
"Asia/Kashgar",
"Asia/Krasnoyarsk",
"Asia/Kuala_Lumpur",
"Asia/Kuching",
"Asia/Macao",
"Asia/Macau",
"Asia/Makassar",
"Asia/Manila",
"Asia/Shanghai",
"Asia/Singapore",
"Asia/Taipei",
"Asia/Ujung_Pandang",
"Asia/Ulaanbaatar",
"Asia/Ulan_Bator",
"Asia/Urumqi",
"Australia/Perth",
"Australia/West",
"CTT",
"Etc/GMT-8",
"Hongkong",
"PRC",
"Singapore",
"Australia/Eucla",
"Asia/Dili",
"Asia/Irkutsk",
"Asia/Jayapura",
"Asia/Pyongyang",
"Asia/Seoul",
"Asia/Tokyo",
"Etc/GMT-9",
"JST",
"Japan",
"Pacific/Palau",
"ROK",
"ACT",
"Australia/Adelaide",
"Australia/Broken_Hill",
"Australia/Darwin",
"Australia/North",
"Australia/South",
"Australia/Yancowinna",
"AET",
"Antarctica/DumontDUrville",
"Asia/Khandyga",
"Asia/Yakutsk",
"Australia/ACT",
"Australia/Brisbane",
"Australia/Canberra",
"Australia/Currie",
"Australia/Hobart",
"Australia/Lindeman",
"Australia/Melbourne",
"Australia/NSW",
"Australia/Queensland",
"Australia/Sydney",
"Australia/Tasmania",
"Australia/Victoria",
"Etc/GMT-10",
"Pacific/Chuuk",
"Pacific/Guam",
"Pacific/Port_Moresby",
"Pacific/Saipan",
"Pacific/Truk",
"Pacific/Yap",
"Australia/LHI",
"Australia/Lord_Howe",
"Antarctica/Macquarie",
"Asia/Sakhalin",
"Asia/Ust-Nera",
"Asia/Vladivostok",
"Etc/GMT-11",
"Pacific/Efate",
"Pacific/Guadalcanal",
"Pacific/Kosrae",
"Pacific/Noumea",
"Pacific/Pohnpei",
"Pacific/Ponape",
"SST",
"Pacific/Norfolk",
"Antarctica/McMurdo",
"Antarctica/South_Pole",
"Asia/Anadyr",
"Asia/Kamchatka",
"Asia/Magadan",
"Etc/GMT-12",
"Kwajalein",
"NST",
"NZ",
"Pacific/Auckland",
"Pacific/Fiji",
"Pacific/Funafuti",
"Pacific/Kwajalein",
"Pacific/Majuro",
"Pacific/Nauru",
"Pacific/Tarawa",
"Pacific/Wake",
"Pacific/Wallis",
"NZ-CHAT",
"Pacific/Chatham",
"Etc/GMT-13",
"MIT",
"Pacific/Apia",
"Pacific/Enderbury",
"Pacific/Fakaofo",
"Pacific/Tongatapu",
"Etc/GMT-14",
"Pacific/Kiritimati",
});



}
