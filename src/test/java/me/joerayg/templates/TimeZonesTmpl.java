package me.joerayg.templates;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javolution.util.FastSortedMap;

public class TimeZonesTmpl {

    public static final String AMERICA__NEW_YORK = "America/New_York";
    public static final String AMERICA__CHICAGO = "America/Chicago";

    private static final List<String> timeZonesList = Arrays
            .asList(new String[] { AMERICA__CHICAGO, AMERICA__NEW_YORK });

    private static final Map<String, String> timeZonesMap = new FastSortedMap<String, String>();

    private TimeZonesTmpl() {
        for (final String timeZone : timeZonesList) {
            timeZonesMap.put(timeZone, timeZone);
        }
    }

    public static List<String> list() {
        return timeZonesList;
    }

    public static Map<String, String> map() {
        return timeZonesMap;
    }

}
